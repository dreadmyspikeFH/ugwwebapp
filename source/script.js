/* Toggle between adding and removing the "responsive" class to the navbar when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("myNavbar");
    if (x.className === "navbar") {
    x.className += " responsive";
    } else {
    x.className = "navbar";
    }
}

function showEvents() {
    var x = document.getElementById("events");
    var y = document.getElementById("map");
    x.style.display = "block";
    y.style.display = "none";
}

function showMap() {
    var x = document.getElementById("events");
    var y = document.getElementById("map");
    y.style.display = "block";
    x.style.display = "none";
    initMap();
}

var globalLat = 48.149299199999994;
var globalLon = 16.301260799999998;
function api_Call(){
    const userAction = async () => {
      //const response = await fetch(`http://37.221.194.244:8080/v1/api/schedule/gps/${parseInt((globalLat*1000000), 10)}/${parseInt((globalLon*1000000), 10)}`);
      //console.log(response)
      const response = await fetch(`http://192.168.0.171:8010/proxy/v1/api/schedule/gps/${parseInt((globalLat*1000000), 10)}/${parseInt((globalLon*1000000), 10)}`);
      const myJson = await response.json(); //extract JSON from the http response
      // do something with myJson
      console.log(myJson);
      console.log(myJson[0].name);
      var index = 0;
      myJson.forEach(element => {
        index++;
        var node = document.createElement("LI");                 // Create a <li> node
        var textnode = document.createTextNode(element.name);         // Create a text node
        node.appendChild(textnode);                              // Append the text to <li>
        node.setAttribute("class", "list-group-item");
        node.setAttribute("id", `liItem${index}`);   
        node.style.fontWeight = "bold";       
        var location = document.createElement("P");
        location.innerHTML = (`${element.loc}, ${parseFloat(calcDistance(element)).toFixed(1)}km`)
        location.style.fontWeight = "normal";
        location.style.float = "right";
        var date = document.createElement("P");
        date.innerHTML = (`${element.von}, ${element.zeit}`)
        date.style.fontWeight = "normal";
        node.appendChild(location)
        node.appendChild(date)
        document.getElementById("eventList").appendChild(node);
      });
    }

    userAction()
}

function calcDistance(obj){
  return getDistanceFromLatLonInKm(globalLat, globalLon, obj.lat/1000000, obj.lon/1000000)
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

var map;
function initMap(lat=48.1, lon=16.3){
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: lon},
    zoom: 16, // typical is usually 8 or 9
    mapTypeControl: true, //default
  });
  var pos = {lat: lat, lng: lon};
  var marker = new google.maps.Marker({position: pos, map: map});
  if(globalLat!=0){
    map.setCenter(new google.maps.LatLng(globalLat, globalLon));
    var poss = {lat: globalLat, lng: globalLon};
    var markerr = new google.maps.Marker({position: poss, map: map});
  }
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  setTimeout("",1000);
  console.log("Latitude: " + position.coords.latitude + 
  " Longitude: " + position.coords.longitude);
  globalLat = position.coords.latitude;
  globalLon = position.coords.longitude;
  console.log(globalLat);
  map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
}