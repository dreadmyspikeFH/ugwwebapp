Web App for UGW

Usage:

Preconditions:
* changing ip address
* there could be need of installing the local-cors-proxy npm package to bypass the cross-origin limitation of the server requesting a cors policy in the response header
* in browsers like safari there could be need of changing the server to https. This works by changing the package.json files entry from "http-server ./source" to "serve ./source". The package is already installed. There will be a problem with mixing content of https and http after that.
* npm install

Starting:
npm start